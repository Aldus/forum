<?php

/**
 *  @module         Forum
 *  @version        see info.php of this module
 *  @authors        Julian Schuh, Bernd Michna, "Herr Rilke", Dietrich Roland Pehlke (last)
 *  @copyright      2004-2018 Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @license        see info.php of this module
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

class forum_interface
{    
   
    static function build_forum_select( $iSectionID, $iParentID, $aOptions=array() )
    {
        $aAllForums = array();
        LEPTON_database::getInstance()->execute_query(
            "SELECT `forumid`,`title`,`parentid` FROM `".TABLE_PREFIX."mod_forum_forum` WHERE `section_id`=".$iSectionID." ORDER BY `displayorder`",
            true,
            $aAllForums,
            true
        );
        
        $sParams = "";
        foreach($aOptions as $key=>$value)
        {
            $sParams .= " ".$key."=\"".$value."\"";
        }
        
        $sHTML = "<select ".$sParams.">\n<option value=\"0\"> - </option>\n";
        foreach($aAllForums as $aForumRef)
        {
            $sHTML .= "\n<option value='".$aForumRef['forumid']."' ".($aForumRef['forumid'] == $iParentID ? "selected='selected'" : "" ).">".$aForumRef['title']."</option>\n";
        }
        $sHTML .= "\n</select>\n";
        
        return $sHTML;
    }
    
    static function print_success( $sMessage, $sRedirectURL )
    {
        $oTWIG = lib_twig_box::getInstance();
        echo $oTWIG->render(
            "@theme/success.lte",
            array(
                "MESSAGE" => $sMessage,
                "REDIRECT" => $sRedirectURL,
                "REDIRECT_TIMER" => REDIRECT_TIMER
            )
        );
        
        return 0;
    }
    
    static function print_error( $sMessage, $sRedirectURL )
    {
        $oTWIG = lib_twig_box::getInstance();
        echo $oTWIG->render(
            "@theme/error.lte",
            array(
                "MESSAGE" => $sMessage,
                "REDIRECT" => $sRedirectURL,
                "REDIRECT_TIMER" => REDIRECT_TIMER
            )
        );
        
        return 0;
    }

}