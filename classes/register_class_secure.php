<?php

/**
 *  @module         Forum
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2018 Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @license        see info.php of this module
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

$files_to_register = array(
	'class_forumcache.php'
);

if(class_exists("LEPTON_secure", true))
{
    LEPTON_secure::getInstance()->accessFiles( $files_to_register );
} else {
    foreach($files_to_register as &$path)
    {
        $path = "/modules/forum/classes/".path;
    }
    
    global $lepton_filemanager;
    if (!is_object($lepton_filemanager))
    {
        require_once "../../../framework/class.lepton.filemanager.php";
    }
    $lepton_filemanager->register( $files_to_register );
}
