<?php

/**
 *  @module         Forum
 *  @version        see info.php of this module
 *  @authors        Julian Schuh, Bernd Michna, "Herr Rilke", Dietrich Roland Pehlke (last)
 *  @copyright      2004-2018 Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @license        see info.php of this module
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

class forum extends LEPTON_abstract
{

    // Own instance for this class!
    static $instance;
 
    // internal storage for the current section_id the forum belongs too
    public $iSectionID = 0;
    
    // All forums for a given section
    public $aAllForums = array();
    
    // The same as 'tree'.
    public $aForumsTree = array();

    public function initialize(  ) 
    {
    
    }
    
    public function getForumsBySection( $iSectionID )
    {
        $this->iSectionID = $iSectionID;
        $this->aAllForums = array();
        LEPTON_database::getInstance()->execute_query(
            "SELECT * FROM `".TABLE_PREFIX."mod_forum_forum` WHERE `section_id`=".$iSectionID." ORDER BY `parentid`,`displayorder`",
            true,
            $this->aAllForums,
            true
        );
        
        $this->aForumsTree = [];
        $this->buildTree( 0, $this->aForumsTree );
    }
    
    private function buildTree( $iParentID, &$aStorage, $iRecursionsDeep = 0  )
    {
        if( 50 < $iRecursionsDeep )
        {
            die("[Forum - 20041] Fatal error - too mutch recursions!" );
        }
        
        $aTempResults = [];
        LEPTON_database::getInstance()->execute_query(
            "SELECT * FROM `".TABLE_PREFIX."mod_forum_forum` WHERE `section_id`=".$this->iSectionID." AND `parentid` = ".$iParentID." ORDER BY `displayorder`",
            true,
            $aTempResults,
            true
        );
        
        foreach($aTempResults as &$ref)
        {
            $ref['root_deep'] = $iRecursionsDeep;
            $ref['children'] = [];
            $this->buildTree( $ref['forumid'], $ref['children'], $iRecursionsDeep+1); 
            
            $aStorage[] = $ref;
        }
    }
}