<?php
/**
 *  @module         Forum
 *  @version        see info.php of this module
 *  @authors        Julian Schuh, Bernd Michna, "Herr Rilke", Dietrich Roland Pehlke (last)
 *  @copyright      2004-2018 Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @license        see info.php of this module
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

require(LEPTON_PATH . '/modules/admin.php');

include_once(LEPTON_PATH .'/framework/summary.module_edit_css.php');

$oFORUM = forum::getInstance();
$oFORUM->getForumsBySection( $section_id );

$MOD_FORUM = $oFORUM->language;

// echo LEPTON_tools::display( $oFORUM->aForumsTree );

if (isset($_REQUEST['forumid'])) {
	
	$forum = array();
	$database->execute_query(
	    "SELECT * FROM `" . TABLE_PREFIX . "mod_forum_forum` WHERE `forumid` = '" . intval($_REQUEST['forumid']) . "' AND `section_id` = '".$section_id."' AND `page_id` = '".$page_id."'",
	    true,
	    $forum,
	    false
	);

    if(0 === count($forum))
    {	
        $admin->print_error(
			"[F 3001] ".$MOD_FORUM['TXT_NO_ACCESS_F'],
			ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'&section_id='.$section_id
		);
	}

	$mode = "edit";

} else {

    $forum = [
        "forumid"       => -1,
        "title"         => "New forum",
        "description"   => "Descripe the new forum here",
        "displayorder"  => 1,
        "parentid"      => 0, // no parent
        "readaccess"    => "reg",
        "writeaccess"   => "reg"
    ];
    
    $mode = "new";
}

/**
 *  Baustelle!
 *
 */
$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule("forum");

echo $oTWIG->render(
    "@forum/edit_forum.lte",
    array(
        "page_id"   => $page_id,
        "section_id" => $section_id,
        "current_forum" => $forum,
        'mode'      => $mode,
        "oFORUM"    => $oFORUM
    )
);
 


$admin->print_footer();
