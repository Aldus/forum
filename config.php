<?php
/**
 *  @module         Forum
 *  @version        see info.php of this module
 *  @authors        Julian Schuh, Bernd Michna, "Herr Rilke", Dietrich Roland Pehlke (last)
 *  @copyright      2004-2018 Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @license        see info.php of this module
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

$settings = array();
$database->execute_query(
    "SELECT * from `".TABLE_PREFIX."mod_forum_settings` WHERE `section_id` = ".$section_id,
    true,
    $settings,
    false
);

if(count($settings) === 0)
{
    $settings = array(
        "section_id"            => $section_id,
        "FORUMDISPLAY_PERPAGE"  => 5,
        "SHOWTHREAD_PERPAGE"    => 5,
        "PAGENAV_SIZES"         => 0,
        "DISPLAY_SUBFORUMS"     => 1,
        "DISPLAY_SUBFORUMS_FORUMDISPLAY"    => 1 ,
        "FORUM_USE_CAPTCHA"     => 1,
        "ADMIN_GROUP_ID"        => 1,   // ?
        "VIEW_FORUM_SEARCH"     => 1,
        "FORUM_MAX_SEARCH_HITS" => 30,
        "FORUM_SENDMAILS_ON_NEW_POSTS"  => 0,
        "FORUM_ADMIN_INFO_ON_NEW_POSTS" => '',
        "FORUM_MAIL_SENDER"     => "name@your-domain.tld" ,
        "FORUM_MAIL_SENDER_REALNAME"    => "Web Site Forum",
        "FORUM_USE_SMILEYS"     => 1,
        "FORUM_HIDE_EDITOR"     => 0,
        "FORUM_USERS"           => ''
    );
    
    $database->build_and_execute(
        "insert",
        TABLE_PREFIX."mod_forum_settings",
        $settings
    );
    
    if($database->is_error())
    {
        die( "[80012]] ".$database->get_error() );
    }
}

// Einträge, die in der Themenübersicht je Seite angezeigt werden sollen
define('FORUMDISPLAY_PERPAGE', $settings['FORUMDISPLAY_PERPAGE']);

// Einträge, die je Seite in einem Thema angezeigt werden sollen
define('SHOWTHREAD_PERPAGE', $settings['SHOWTHREAD_PERPAGE']);

// Legt fest, ob für die Zahlen in der Seitennavigation verschiedene Schriftgröﬂen verwendet werden sollen
define('PAGENAV_SIZES', $settings['PAGENAV_SIZES']);

// Unterforen auf der Foren-Startseite anzeigen?
define('DISPLAY_SUBFORUMS', $settings['DISPLAY_SUBFORUMS']);

// Unterforen in der Themenübersicht anzeigen?
define('DISPLAY_SUBFORUMS_FORUMDISPLAY', $settings['DISPLAY_SUBFORUMS_FORUMDISPLAY']);

// Sollen für Gäste Captchas zum schreiben verwendet werden?
define('FORUM_USE_CAPTCHA', $settings['FORUM_USE_CAPTCHA']);

// ID der Gruppe der Administratoren (Dürfen Beiträge + Themen ändern/löschen)
define('ADMIN_GROUP_ID', $settings['ADMIN_GROUP_ID']);

// Soll das Suchformular angezeigt werden ?
define('VIEW_FORUM_SEARCH', $settings['VIEW_FORUM_SEARCH']);

// max. Ausgabe von x Treffern in der Suchfunktion
define('FORUM_MAX_SEARCH_HITS', $settings['FORUM_MAX_SEARCH_HITS']);

// sollen Mails versendet werden, wenn neue Posts eingehen?
define('FORUM_SENDMAILS_ON_NEW_POSTS', $settings['FORUM_SENDMAILS_ON_NEW_POSTS']);

// Diese Adresse bei neuen Beiträgen informieren?'
define('FORUM_ADMIN_INFO_ON_NEW_POSTS', $settings['FORUM_ADMIN_INFO_ON_NEW_POSTS']);

// Sender of notification emails on new posts
define('FORUM_MAIL_SENDER', $settings['FORUM_MAIL_SENDER']);

// Sender's name
define('FORUM_MAIL_SENDER_REALNAME', $settings['FORUM_MAIL_SENDER_REALNAME']);

// use smileys
define('FORUM_USE_SMILEYS', $settings['FORUM_USE_SMILEYS']);

// show hide/unhide button instead of post editor
define('FORUM_HIDE_EDITOR', $settings['FORUM_HIDE_EDITOR']);

// remember user data
define('FORUM_USERS', $settings['FORUM_USERS']);
