<?php

/**
 *  @module         Forum
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2018 Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @license        see info.php of this module
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */


$files_to_register = array(
	'save_settings.php',
	'add.php',
	'addedit_forum.php',
	'backend.php',
	'class_forumcache.php',
	'config.php',
	'content.php',
	'edit_post.php',
	'forum_view.php',
	'functions.php',
	'help.php',
	'include_search.LIKE-Version.php',
	'include_search.php',
	'include_searchform.php',
	'include_sendmails.php',
	'insertupdate_forum.php',
	'install.php',
	'modify_settings.php',
	'modify.php',
	'pagination.php',
	'post_delete.php',
	'post_edit.php',
	'save_post.php',
	'save_settings.php',
	'searchtheforum.php',
	'smilies.php',
	'thread_create.php',
	'thread_reply.php',
	'thread_view.php',
	'uninstall.php',
	'upgrade.php',
	'view.php'
);

if(class_exists("LEPTON_secure", true))
{
    LEPTON_secure::getInstance()->accessFiles( $files_to_register );
} else {
    foreach($files_to_register as &$path)
    {
        $path = "/modules/forum/".path;
    }
    
    global $lepton_filemanager;
    if (!is_object($lepton_filemanager))
    {
        require_once "../../framework/class.lepton.filemanager.php";
    }
    $lepton_filemanager->register( $files_to_register );
}

?>