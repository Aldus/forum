<?php
/**
 *  @module         Forum
 *  @version        see info.php of this module
 *  @authors        Julian Schuh, Bernd Michna, "Herr Rilke", Dietrich Roland Pehlke (last)
 *  @copyright      2004-2018 Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @license        see info.php of this module
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

if(!defined('SKIP_CACHE')) define('SKIP_CACHE', 1);
require_once(LEPTON_PATH . '/modules/forum/backend.php');

$oFORUM = forum::getInstance();
$oFORUM->getForumsBySection( $section_id );

$message = (0 === count($oFORUM->aForumsTree))
    ? $oFORUM->language['TXT_NO_FORUMS_B']
    : ""
    ;
    
/**
 *	Collecting the values/datas for the page
 */
$page_data = array(
	'LEPTON_PATH' => LEPTON_PATH,
	'LEPTON_URL' => LEPTON_URL,
	'section_id'    => $section_id,
	'page_id'       => $page_id,
	'oFORUM'        => $oFORUM,
	'message'       => $message
);

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule("forum");

echo $oTWIG->render(
	"@forum/modify.lte",
	$page_data
);

?>
