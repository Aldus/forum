### Forum
============

Nothing more and nothing less than a simple brief forum for LEPTON CMS, based on WB Forum 0.5.7. 


#### Requirements

* [LEPTON CMS][1], Version >= 3.x


#### Installation

* download latest [.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon you are done. <br />
Create a new page with this addon.

#### Changelog

see commits on gitlab at https://gitlab.com/Aldus/forum

[1]: https://lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com/lepador/modules/forum.php
